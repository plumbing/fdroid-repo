FROM registry.gitlab.com/fdroid/docker-executable-fdroidserver:latest 
RUN apt-get update
RUN apt-get install -y --no-install-recommends ssh
RUN rm -rf /var/lib/apt/lists/*
RUN pip3 install fdroidserver
Run mkdir -p /usr/local/share/doc && ln -s /usr/share/doc/fdroidserver /usr/local/share/doc/fdroidserver

